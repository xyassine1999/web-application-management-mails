@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Responsive vertical menu navigation</title>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:700, 600,500,400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="/css/main.css">

        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="/js/main.js"></script>
    </head>
    <body>
        <div class="header">
            <div class="logo">
                <i class="fa fa-tachometer"></i>
                <span>Brand</span>
            </div>
            <a href="#" class="nav-trigger"><span></span></a>
        </div>
        <div class="side-nav">
            <div class="logo">
                <i class="fa fa-tachometer"></i>
                <span>MENU</span>
            </div>
            <nav>
                <ul>
                    <li>
                        <a href="#">
                            <span><i class="fa fa-user"></i></span>
                            <span>ENVOYER COURRIEL</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">

                            <span><i class="fa fa-envelope"></i></span>
                            <span>BOITE MESSAGE</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">
                            <span><i class="fa fa-bar-chart"></i></span>
                            <span>IMPRIMER RECU</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span><i class="fa fa-credit-card-alt"></i></span>
                            <span>OTHER SITES</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        </div>
    </body>
</html>
@endsection
