<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
   return view('home');
});
Route::get('/auth/login.blade.php', function () {
   return view('auth.login');
});
Route::get('/auth/register.blade.php', function () {
   return view('auth.register');
});
Route::auth(); 
Route::get('/home/welcome', function () {
   return view('home.welcome');
});
