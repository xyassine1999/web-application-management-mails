-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2018 at 10:04 PM
-- Server version: 5.7.21-log
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gestion-courriels`
--

-- --------------------------------------------------------

--
-- Table structure for table `courriels`
--

CREATE TABLE IF NOT EXISTS `courriels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `mail` varchar(110) NOT NULL,
  `message` text NOT NULL,
  `destination` text NOT NULL,
  `upload` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NomDestination` text NOT NULL,
  `NomProvenence` text NOT NULL,
  `Mail` varchar(101) NOT NULL,
  `phone` varchar(104) NOT NULL,
  `Message` text NOT NULL,
  `upload` longblob NOT NULL,
  `Date` varchar(1111) NOT NULL,
  `Time` int(11) NOT NULL,
  `open` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `NomDestination`, `NomProvenence`, `Mail`, `phone`, `Message`, `upload`, `Date`, `Time`, `open`) VALUES
(19, 'bouchaib naimy', '0', 'jamal@gmail.com', '0620179224', 'salam cv boss ?', 0x433a5c77616d705c746d705c706870374644342e746d70, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `messagerie`
--

CREATE TABLE IF NOT EXISTS `messagerie` (
  `Nom` text NOT NULL,
  `Mail` varchar(11) NOT NULL,
  `Email` varchar(11) NOT NULL,
  `Message` text NOT NULL,
  `Date` date NOT NULL,
  `Time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2018_10_02_064923_create_protect_table', 1),
('2018_11_24_171103_create_sessions_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jamalxap@gmail.com', 'd76d6d765b85d3ef6d1831ea442ce9629412fdf84ac1e183d415630fb49df3c7', '2018-10-03 09:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `protect`
--

CREATE TABLE IF NOT EXISTS `protect` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'bouchaib naimy', 'bouchaibnaimy1999@gmail.com', '$2y$10$pIAOT6SL.YpEJScLHbEYQudz/IfNMPuanFcMNEdKepvscaenohVcm', 'zaRGTpWjmI8AlICqsrjSUQHXG0xZtzjQoSsfOIj9kGwAuDMOjnDatNrYe3QJ', '2018-10-02 06:02:38', '2018-11-29 19:20:46'),
(2, 'jamal', 'jamalxap@gmail.com', '$2y$10$99Dnt2olXMHXYq9XoBdMUedScgKZls3PJgxrHYZv5YtCHqB1yJ4GS', 'xe1cYTw0ZWmZTExjOzsdYK0w05MgubFA8DEsmNftSkvQzjLQErofhtTZdig5', '2018-10-02 07:12:16', '2018-11-29 19:20:18'),
(3, 'utilisateur 1', 'boch213ask@gmail.com', '$2y$10$znpcqbqAUooSJEAdnqNNcONEGpsVDbNsQoItv6hl35MLHgwT1FYay', 'P9WckK2x0LUOFs6JtjR0fBGPYiWrfaal1v3v9rKiLpuH8tr1W1P0SshH66Hg', '2018-10-08 19:25:11', '2018-10-11 08:48:15'),
(4, 'sissani', 'sissani@gmail.com', '$2y$10$EiVF3gwHacWS6KV2F9e0Qu2xshywPWkiVzht8RKeYgqVGJ5iiX/rS', '1lkdbbSrqBTzYWRHRABvxp7NxeVMhxJrlnqFQPVz5yjzgSPEdt6Ebk2FMyAn', '2018-10-11 09:57:23', '2018-10-15 15:02:12'),
(5, 'hassan', 'said@gmail.com', '$2y$10$zcbZJjRkVv9XdWnq7HT66OcJj4HwFQ5.bSlK55i.YjIEdmM8TWk5K', NULL, '2018-11-24 11:56:07', '2018-11-24 11:56:07'),
(6, 'ziko', 'ziko@gmail.com', '$2y$10$T8.vsaguYIgIc3ViEwnyge0/gOwo6pL175nmfIOaN45RrNYt11cyK', 'n0B1unkUN0l10AedOOlfgX1jJtml85CfrGVpws3Hl7Q3OFpP6IeMCJv6T8jN', '2018-11-24 12:08:29', '2018-11-26 13:10:02'),
(7, 'soukaina', 'soukaina@gmail.com', '$2y$10$8ksRCzKs8xNSxPMN.amcxOJTD56JcdSEzlac88lRFLg2pFElKXx3e', 'FndEmV4UZilwRCMD4e4uIOMmZwXeqae1ihI0ixmrM4OD4bUhbXEHddd8PPN3', '2018-11-24 17:12:19', '2018-11-25 11:15:09'),
(8, 'Choaib', 'bouchaib1999@gmail.com', '$2y$10$7WQJ5EtPRcLAFvNanRC2PeGds9THHxk8FWL1woTQp25vRpTvwfPDm', 'aLU3AG9xbK6kY2ctmLyaCIbRb7tLQK3FsCrHhdKdHRMrQVgPqfPEabSsucv0', '2018-11-24 20:09:49', '2018-11-24 20:09:56'),
(9, 'Jihane', 'jihane@gmail.com', '$2y$10$IzyOIG0hWQuNX2mZ1mLF.ukmw3zJSe6IkeOnCaUGe76C4Icdztgm2', NULL, '2018-11-24 20:11:06', '2018-11-24 20:11:06');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
