@extends('layouts.app1')

@section('content')
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Responsive vertical menu navigation</title>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:700, 600,500,400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="/css/main.css">

        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="/js/main.js"></script>
    </head>
    <body>
        <div class="header">
            <div class="logo">
                <i class="fa fa-tachometer"></i>
                <span>Brand</span>
            </div>
            <a href="#" class="nav-trigger"><span></span></a>
        </div>
        <div class="side-nav">
            <div class="logo">
                <i class="fa fa-tachometer"></i>
                <span>MENU</span>
            </div>
            <nav>
                <ul>
                    <li>
    <input style="background-color:lightgreen" type="button" class="boutonSpoiler" value="ENVOYER COURRIEL" onclick="ouvrirFermerSpoiler(this);" />
    <div class="spoiler">
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="/create" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <div class="form-group">
                                <div class="col-md-6">
                                    <label>Destination</label>
                         <input type="text" class="form-control" name="nomdestination" value="nom">
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Provenence</label>
                                    <input type="text" class="form-control" name="nomprovenence" value="nom">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="prenomdestination" value="prenom">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                              <input type="text" class="form-control" name="prenomprovenence" value="prenom">
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                              <input type="text" class="form-control" name="mail" value="mail">
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                 <select name="provenence">
                 <option value="chef">chef</option>
                 <option value="chef-sup">chef-sup</option>
                 <option value="directeur">directeur</option>
                 <option value="president">president</option>
                  </select>
              </div>
               </div>
              <div class="form-group">
              <div class="col-md-6">
              <input type="file" id="myFile" name="upload">
          </div>
      </div>
      <div class="form-group">
                                <div class="col-md-6">
                 <select name="destination">
                 <option value="chef">chef</option>
                 <option value="chef-sup">chef-sup</option>
                 <option value="directeur">directeur</option>
                 <option value="president">president</option>
                  </select>
              </div>
               </div>
 <script>
function myFunction() {
    var x = document.getElementById("myFile");
    x.disabled = true;
}
</script>
            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        envoyer
                                    </button>
                                </div>
                            </div>
            </form>
</div>
<style>
div.spoiler {
    padding: 20px 10px 10px 10px;
    width: 500%;
    border: 1px solid black;
    position: absolute;
    left: 250px;
}
div.option{
position: absolute;
    top: 420px;
    left: 280px;


}
 
div.spoiler input.boutonSpoiler {
    position: absolute;
    top: -10px;
    left: 10px;
}
 
div.spoiler div.contenuSpoiler {
    display: none;
}
</style>
<script>
    function ouvrirFermerSpoiler(bouton) {
    var divContenu = bouton.nextSibling;
    if(divContenu.nodeType == 3) divContenu = divContenu.nextSibling;
    if(divContenu.style.display == 'block') {
        divContenu.style.display = 'none';
    } else {
        divContenu.style.display = 'block';
    }
}
</script>
                    </li>
                    <li>
<input style="background-color:lightgreen" type="button" class="boutonSpoiler" value="Boite Mail" onclick="ouvrirFermerSpoiler(this);" />
                    </li>
                    <li class="active">
                        <input style="background-color:lightgreen" type="button" class="boutonSpoiler" value="Imprimer Recu" onclick="ouvrirFermerSpoiler(this);" />
                    </li>
                    <li>
<span><input style="background-color:lightgreen" type="button" class="boutonSpoiler" value="Other sites" onclick="ouvrirFermerSpoiler(this);" />
                    </li>
                </ul>
            </nav>
        </div>
        </div>
        <link rel="stylesheet" type="text/css" href="http://www.arabic-keyboard.org/keyboard/keyboard.css"> 
    <script type="text/javascript" src="http://www.arabic-keyboard.org/keyboard/keyboard.js" charset="UTF-8"></script>

</body>
</html>
@endsection

